import React, { Component } from 'react'
import { StyleSheet, View, ImageBackground, Image, Dimensions, ScrollView, TouchableOpacity, FlatList } from 'react-native'

import Text from '../components/Text'
import { theme, urls} from '../constants';
import { withNavigation } from 'react-navigation';
import axios from 'axios'

class Details extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerStyle: {
        backgroundColor: theme.colors.white,
      },
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      enterprise: {},
      photo: {uri: '../assets/img/enterprise1.png'}
    };
    
    this.getEnterpriseById();
  }

  render () {
    const { navigation } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.profileContainer}>
          <Image
            style={styles.avatar}
            source={this.state.photo}
          />

          <View style={styles.profileInfo}>
            <Text h1 bold style={styles.name}>{this.state.enterprise.enterprise_name}</Text>
            {/* <Text style={styles.bio}>{this.state.enterprise.enterprise_type.enterprise_type_name}</Text> */}

            <View style={styles.buttonContainer}>
              <Text style={styles.firstButton}>{this.state.enterprise.city}, </Text>
              <Text>{this.state.enterprise.country}</Text>
            </View>
          </View>
        </View>
        <View style={{
          flex: 1, flexDirection: 'row', justifyContent: 'center',
          alignItems: 'stretch', marginTop: 20
        }}>
          <View style={{ width: 100, height: 100, justifyContent: 'center' }}>
            <Text h2 >{this.state.enterprise.shares}</Text>
            <Text gray >Ações</Text>
          </View>
          <View style={{ width: 100, height: 100, justifyContent: 'center' }}>
            <Text h2 >{this.state.enterprise.share_price}</Text>
            <Text gray >Preços</Text>
          </View>
          <View style={{ width: 100, height: 100, justifyContent: 'center' }} >
            <Text h2 >{this.state.enterprise.own_shares}</Text>
            <Text gray >Shares Price</Text>
          </View>
        </View>
        <View style={styles.description}>
          <View style={{ height: 200 }} >
            <Text h2 semibold> Descrição</Text>
            <Text h3 regular>{this.state.enterprise.description}</Text>
          </View>
        </View>
        <View style={styles.icons}>
          <View style={{ width: 50, height: 50 }} >
            <Image
              source={require('../assets/icons/facebook.png')}>
            </Image>
          </View>
          <View style={{ width: 50, height: 50 }} >
            <Image
              source={require('../assets/icons/twitter.png')}>
            </Image>
          </View>
          <View style={{ width: 50, height: 50 }} >
            <Image
              source={require('../assets/icons/linkedin.png')}>
            </Image>
          </View>
          <View style={{ width: 50, height: 50 }} >
            <Image
              source={require('../assets/icons/email.png')}>
            </Image>
          </View>
          <View style={{ width: 50, height: 50 }} >
            <Image
              source={require('../assets/icons/phone.png')}>
            </Image>
          </View>
          <View>
          </View>
        </View>
      </View>
    )
  }

  getEnterpriseById () {
    const { navigation } = this.props;

    axios.get(urls.urls.API + 'enterprises/' + navigation.getParam('enterprise_id'), {
      method: 'GET',
      headers: {
        'access-token': navigation.getParam('oAuth')['access-token'],
        'client': navigation.getParam('oAuth')['client'],
        'uid': navigation.getParam('oAuth')['uid']
      }
    }).then((response) => {
      this.setState({
        enterprise: response.data.enterprise,
        photo: {uri: response.data.enterprise.photo}
      })
    }).catch(
      (error) => {
        console.log(error);
      }
    )
  }
}

const styles = StyleSheet.create({

  container: {
    padding: 15,
    backgroundColor: theme.colors.white,
    borderBottomWidth: 1,
    borderColor: theme.colors.white,

  },
  profileContainer: {
    flexDirection: 'row',
  },
  avatar: {
    width: 68,
    height: 68,
    borderRadius: 34,
    marginRight: 15,
  },

  profileInfo: {
    flex: 1,
  },
  name: {
    marginTop: 5,

  },
  bio: {
    marginTop: 5,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginTop: 5,
  },
  description: {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
    marginTop: 250
  },
  icons: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
    marginTop: 100,
  }
});
export default withNavigation(Details);