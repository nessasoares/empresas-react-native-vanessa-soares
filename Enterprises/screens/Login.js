import React, { Component } from 'react'

import { View, TextInput, StyleSheet, TouchableOpacity } from 'react-native';

import { withNavigation } from "react-navigation";

import Text from '../components/Text'

import { theme, urls } from '../constants'

import axios from 'axios'


class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Text h1 semibold white center>Enterprises</Text>
        </View>
        <View style={styles.formContainer}>
          <TextInput
            style={styles.input}
            placeholder='Email'
            placeholderTextColor='white'
            onChangeText={(email) => this.setState({ email })}
            value={this.state.user}
          />
          <TextInput style={styles.input}
            placeholder='Password'
            placeholderTextColor='white'
            onChangeText={(password) => this.setState({ password })}
            value={this.state.password}
            secureTextEntry={true}
          />
        </View>
        <View style={styles.loginButtonSection}>
          <TouchableOpacity style={styles.button}
            onPress={this.login}
          >
            <Text h1 bold center> Login </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  login = () => {
    axios.post(urls.urls.API + 'users/auth/sign_in',
    {
      email: this.state.email,
      password: this.state.password
    }).then(
      (response) => {
        if (response.headers['Status'] = 200) {
          this.props.navigation.navigate('List', {
            'access-token': response.headers['access-token'],
            'client': response.headers['client'],
            'uid': response.headers['uid']
          })
        }

      }
    ).catch(
      (error) => {
        console.log(error);
      }
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'gray'
  },
  formContainer: {
    padding: 20,
  },

  button: {
    backgroundColor: theme.colors.white,
    textAlign: 'center',
    width: 300,
    height: 40,
  },
  loginButtonSection: {
    height: '30%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    borderRadius: 5,
    height: 40,
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
    marginTop: 20,
    paddingHorizontal: 20
  },
});

export default withNavigation(Login);