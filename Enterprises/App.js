
import React, { Component } from 'react';

import Login from './screens/Login'

import { createStackNavigator, createAppContainer } from "react-navigation";

import ListScreen from './components/List'
import DetailsSreen from './screens/Details'

import {
  StyleSheet,
  View,
} from 'react-native';

GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;

class LoginScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerStyle: {
        height: 0 
      },
    };
  };

  render() {
    return (
      <Login></Login>
    );
  }
}

const AppNavigator = createStackNavigator(
  {
    Login: LoginScreen,
    List: ListScreen,
    Details: DetailsSreen
  
  },
  {
    initialRouteName: "Login"
  }
);

const styles = StyleSheet.create({

});

export default createAppContainer(AppNavigator);