import React, { Component } from "react";
import { Text, StyleSheet } from "react-native";

import { theme } from "../constants";

export default class Typography extends Component {
  render() {
    const {
      h1,
      h2,
      h3,
      title,
      body,
      size,
      align,
      regular,
      bold,
      semibold,
      center,
      right,
      height, 
      color,
      primary,
      black,
      white,
      gray,
      style,
      children,
      ...props
    } = this.props;

    const textStyles = [
      styles.text,
      h1 && styles.h1,
      h2 && styles.h2,
      h3 && styles.h3,
      title && styles.title,
      body && styles.body,
      size && { fontSize: size },
      align && { textAlign: align },
      height && { lineHeight: height },
      regular && styles.regular,
      bold && styles.bold,
      semibold && styles.semibold,
      center && styles.center,
      right && styles.right,
      color && styles[color],
      color && !styles[color] && { color },
      primary && styles.primary,
      black && styles.black,
      white && styles.white,
      gray && styles.gray,
      
      style 
    ];

    return (
      <Text style={textStyles} {...props}>
        {children}
      </Text>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    fontSize: theme.sizes.font,
    color: theme.colors.black
  },
  regular: {
    fontWeight: "normal",
  },
  bold: {
    fontWeight: "bold",
  },
  semibold: {
    fontWeight: "500",
  },

  center: { textAlign: "center" },
  
  right: { textAlign: "right" },
  primary: { color: theme.colors.primary },
  black: { color: theme.colors.black },
  white: { color: theme.colors.white },
  gray: { color: theme.colors.gray },
 
  h1: theme.fonts.h1,
  h2: theme.fonts.h2,
  title: theme.fonts.title,
  body: theme.fonts.body,
});