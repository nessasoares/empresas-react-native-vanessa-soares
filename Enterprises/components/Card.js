
import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { withNavigation } from "react-navigation";
import Text from '../components/Text'
import { theme, urls} from '../constants'

class Card extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.card} onPress={() => this.props.navigation.navigate('Details', { enterprise_id: this.props.enterprise_id, oAuth: this.props.oAuth})}>
          <Image
            style={styles.avatar}
            source={urls.urls.PHOTO_API + this.props.photo_url}
          />
          <Text h2 regular style={styles.text}> {this.props.name} </Text>
          <Text h2 regular right style={styles.text}> {this.props.type} </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.white,
    borderBottomWidth: 1,
  },
  card: {
    flexDirection: 'row',
    borderColor: theme.colors.gray,
    backgroundColor: theme.colors.gray

  },
  avatar: {
    width: 32,
    height: 32,
    borderRadius: theme.sizes.radius*6,
    marginTop: 15,
    marginLeft: 15,
  },
  text: {
    padding: 8,
    marginTop: 10,
    marginBottom: 10
  },

});

export default withNavigation(Card);
