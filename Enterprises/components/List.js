// @flow
/*
   NOTE: This file was auto-generated for a component
   named "List"; it is intended to be modified as
   needed to be useful.
*/

import React from 'react'
import { Component } from 'react'
import Card from './Card'
import axios from 'axios'

import {
  View,
  TextInput,
  StyleSheet,
  Image,
  FlatList,
  Picker
} from 'react-native';
import symbolicateStackTrace from 'react-native/Libraries/Core/Devtools/symbolicateStackTrace';
import { urls } from '../constants';
import { withNavigation } from 'react-navigation';

class ListScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerStyle: {

        height: 0
      },
      headerLeft: null,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      enterprises: [],
      name: '',
      type: 1,
      // types: Array.apply(null, { length: 30 }).map(Number.call, Number)
    };

    this.getEnterprises();
  }

  searchEnterprise() {
    const { navigation } = this.props;

    axios.get(urls.urls.API + 'enterprises?name=' + this.state.name, {
      method: 'GET',
      headers: {
        'access-token': navigation.getParam('access-token'),
        'client': navigation.getParam('client'),
        'uid': navigation.getParam('uid')
      }
    }).then((response) => {
      this.setState({
        enterprises: response.data.enterprises
      })
    }).catch(
      (error) => {
        console.log(error);
      }
    )
  }

  render() {
    const { navigation } = this.props;
    // let types = this.state.types.map((value, index) => {
    //   return <Picker.Item key={index} value={value} label={value.toString()} />
    // });

    return (
      <View style={styles.containerStyle}>
        <View style={styles.input} >
          <TextInput
            placeholder='Pequise por Nome'
            placeholderTextColor='black'
            maxLength={40}
            onChangeText={(name) => { this.setState({ name }); this.searchEnterprise() }}
            value={this.state.name}
          >
          </TextInput>
        </View>
        {/* <View style={styles.picker}>
          <Picker>
            {types}
          </Picker>
        </View> */}
        <FlatList
          data={this.state.enterprises}
          keyExtractor={(item, index) => 'key' + index}
          renderItem={({ item }) =>
            <Card
              photo_url={item.photo}
              oAuth={{
                'access-token': navigation.getParam('access-token'),
                'client': navigation.getParam('client'),
                'uid': navigation.getParam('uid')
              }} name={item.enterprise_name} type={item.enterprise_type_name} enterprise_id={item.id} ></Card>}
        />
      </View>
    );
  }

  getEnterprises() {
    const { navigation } = this.props;

    axios.get(urls.urls.API + 'enterprises', {
      method: 'GET',
      headers: {
        'access-token': navigation.getParam('access-token'),
        'client': navigation.getParam('client'),
        'uid': navigation.getParam('uid')
      }
    }).then((response) => {
      this.setState({
        enterprises: response.data.enterprises
      })
    }).catch(
      (error) => {
        console.log(error);
      }
    )
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',

  },
  input: {
    height: 50,
    marginRight: 5,
    marginLeft: 5,
    marginTop: 10,
    backgroundColor: '#CCCCCB',
    marginBottom: 5
  },
  picker: {
    width: 250,
    borderRadius: 4,
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 50,
    height: 50,
    backgroundColor: "#CCCCCB"
  },
  filter: {
    marginTop: 15
  }
});

export default withNavigation(ListScreen);
